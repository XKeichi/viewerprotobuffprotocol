#include "tps_header_protocol.h"

void TObject_Measure_Protocol::Serialize(std::string &rBuffer)
{
    FKadr.SerializeToString(&rBuffer);   
}

void TObject_Measure_Protocol::DebugDesplayKadr()
{
    FKadr.PrintDebugString();
}
