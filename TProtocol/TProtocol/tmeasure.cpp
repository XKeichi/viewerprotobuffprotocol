#include "tmeasure.h"

TMeasure::TMeasure(pspProtocol::Mesure *aMesure):FMesure(aMesure)
{

}

TMeasure::TMeasure(const TMeasure &aValue)
{
    FMesure = new pspProtocol::Mesure();
    FMesure->set_value(aValue.FMesure->value());
    FMesure->set_current(aValue.FMesure->current());
}

float TMeasure::Amperage()const
{
    return FMesure->current();
}

float TMeasure::Voltage()const
{
    return FMesure->value();
}

QString TMeasure::AmperageDisplayValue(unsigned int aPrecision)const
{
    return QString::number(FMesure->current(),'f',aPrecision);
}

QString TMeasure::VoltageDisplayValue(unsigned int aPrecision)const
{
    return QString::number(FMesure->value(),'f',aPrecision);
}

void TMeasure::Amperage(float aValue)
{
    FMesure->set_current(aValue);
}

void TMeasure::Voltage(float aValue)
{
    FMesure->set_value(aValue);
}

bool TMeasure::operator == (const TMeasure& aValue)const
{
    if((FMesure->current()==aValue.FMesure->current())&&(FMesure->value()==aValue.FMesure->value()))
        return true;
    return false;
}

bool TMeasure::operator != (const TMeasure& aValue)const
{
    if((FMesure->current()==aValue.FMesure->current())&&(FMesure->value()==aValue.FMesure->value()))
        return false;
    return true;
}

TMeasure& TMeasure::operator = (const TMeasure &aValue)
{
    this->FMesure->set_value(aValue.FMesure->value());
    this->FMesure->set_current(aValue.FMesure->current());
    return *this;
}
