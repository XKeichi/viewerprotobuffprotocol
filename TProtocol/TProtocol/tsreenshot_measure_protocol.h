#ifndef TSREENSHOT_MEASURE_PROTOCOL_H
#define TSREENSHOT_MEASURE_PROTOCOL_H

#include <QImage>
#include "tps_header_protocol.h"

class TScreenshot_Measure_Value
{
public:
    TScreenshot_Measure_Value(pspProtocol::ScreenshotTypeBuffer* aBuffer);
    const QImage& Screenshot()const;
    bool Screenshot(QString aFileName);
    bool Screenshot(const QImage& aValue);
private:
    pspProtocol::ScreenshotTypeBuffer* FBuffer;
    QImage FImage;
};

class TScreenshot_Measure_Protocol : public Tps_Header_Protocol<TScreenshot_Measure_Value>
{
public:
    explicit TScreenshot_Measure_Protocol(const QString &aDevece, const QString &aVersion);
    explicit TScreenshot_Measure_Protocol(pspProtocol::Kadr &aKadr);
    virtual ~TScreenshot_Measure_Protocol();
    Tps_Header_Protocol::TType Type()const override final;
    TScreenshot_Measure_Value* Content()override;
private:
    TScreenshot_Measure_Value *FScreenshot;
};

#endif // TSREENSHOT_MEASURE_PROTOCOL_H
