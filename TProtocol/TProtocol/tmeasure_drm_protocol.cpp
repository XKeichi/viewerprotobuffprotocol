#include "tmeasure_drm_protocol.h"
using namespace pspProtocol;

TList_Measure_Value::TList_Measure_Value(DRMTypeBuffer *aBuffer):FBuffer(aBuffer)
{
    FItems.resize(aBuffer->value_size());
    for(int i=0;i<aBuffer->value_size();++i)
        FItems[i]=new TMeasure(aBuffer->mutable_value(i));

}

TList_Measure_Value::~TList_Measure_Value()
{
    for(auto *&jItem : FItems)
    {
        delete jItem;
        jItem = NULL;
    }
    FItems.clear();
}

const QVector<TMeasure*>& TList_Measure_Value::Items()const
{
    return FItems;
}

TMeasure* TList_Measure_Value::CreateNewMeasure()
{
    FItems.push_back(new TMeasure(FBuffer->add_value()));
    return FItems.last();
}

TMeasure_DRM_Protocol::TMeasure_DRM_Protocol(const QString &aDevece, const QString &aVersion):Tps_Header_Protocol(aDevece,aVersion)
{
    FKadr.set_type(pspProtocol::Kadr::DRM);
    pspProtocol::DRMTypeBuffer *iBuffer = nullptr;   
    iBuffer = FKadr.mutable_drmmesure();
    FItems = new TList_Measure_Value(iBuffer);    
}

TMeasure_DRM_Protocol::TMeasure_DRM_Protocol(const pspProtocol::Kadr &aKadr)
{
    FKadr = aKadr;
    pspProtocol::DRMTypeBuffer *iBuffer = nullptr;
    iBuffer = FKadr.mutable_drmmesure();
    FItems = new TList_Measure_Value(iBuffer);
}

TMeasure_DRM_Protocol::~TMeasure_DRM_Protocol()
{
    delete FItems;
}

TList_Measure_Value* TMeasure_DRM_Protocol::Content()
{
    return FItems;
}

Tps_Header_Protocol<TList_Measure_Value>::TType TMeasure_DRM_Protocol::Type()const
{
    return ptDRM;
}
