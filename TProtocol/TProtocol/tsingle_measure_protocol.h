#ifndef TSINGLE_MEASURE_PROTOCOL_H
#define TSINGLE_MEASURE_PROTOCOL_H

#include "tps_header_protocol.h"
#include "tmeasure.h"

class TSingleMeasureProtocol : public Tps_Header_Protocol<TMeasure>
{
public:
    //explicit TSingleMeasureProtocol(QString aDevece, QString aVersion):Tps_Header_Protocol(aDevece,aVersion){}
    explicit TSingleMeasureProtocol(const QString &aDevece, const QString &aVersion);
    TSingleMeasureProtocol(const pspProtocol::Kadr &aKadr);
    virtual ~TSingleMeasureProtocol();
    virtual TMeasure* Content()override;
    TType Type()const override final;
private:
    TMeasure *FMeasure;
};

#endif // TSINGLE_MEASURE_PROTOCOL_H
