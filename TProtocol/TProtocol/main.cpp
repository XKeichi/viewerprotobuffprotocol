#include <QCoreApplication>
#include <QTextCodec>
#include <iostream>
#include <memory>
//#define Debug
#include "tps_device_protocol.h"
using namespace pspProtocol;
#include <vector>
#include <QFileInfo>
TObject_Measure_Protocol* Protocol_1_()
{
    TSingleMeasureProtocol* iProtocol = new TSingleMeasureProtocol("Мультиметр","0.a");
    iProtocol->Content()->Amperage(3);
    iProtocol->Content()->Voltage(220);
    return iProtocol;
}

TObject_Measure_Protocol* Protocol_2_()
{
    Trippler_Mesure_Protocol* iProtocol = new Trippler_Mesure_Protocol("Мультиметр","0.b");
    iProtocol->Content()->Frist_Measure_Value()->Amperage(1);
    iProtocol->Content()->Frist_Measure_Value()->Voltage(240);
    iProtocol->Content()->Second_Measure_Value()->Amperage(2);
    iProtocol->Content()->Second_Measure_Value()->Voltage(240);
    iProtocol->Content()->Third_Measure_Value()->Amperage(3);
    iProtocol->Content()->Third_Measure_Value()->Voltage(240);
    return iProtocol;


}
#include <math.h>
TObject_Measure_Protocol* Protocol_3_()
{
    TMeasure_DRM_Protocol* iProtocol = new TMeasure_DRM_Protocol("Мультиметр","0.c");;
    for(int i=1;i<100000;++i)
    {
        TMeasure* iMesure = iProtocol->Content()->CreateNewMeasure();
        iMesure->Voltage(i);
        iMesure->Amperage((float)i/pow(10,(int)log10(i)));
    }

    return iProtocol;

}

TObject_Measure_Protocol* Protocol_4_(QString aPath)
{
    TScreenshot_Measure_Protocol* iProtocol = new TScreenshot_Measure_Protocol("Мультиметр","0.d");    
    aPath+="/BBYZXdw.jpg";
    if(iProtocol->Content()->Screenshot(aPath)==false)
    {
        std::cout<<"Ошибка загрузки картинки"<<std::endl;
        return nullptr;
    }

    return iProtocol;
}
using namespace std;
int main(int argc, char *argv[])
{

    TObject_Measure_Protocol *iProtocol;
    QString aPath = QString::fromLocal8Bit(argv[0]);
    aPath = QFileInfo(aPath).path();


    int iType=-1;
    if(1<argc)
    {
        iType = atoi(argv[1]);//fork c getopt ближе к стилю вызова..., в тз на это нет описания...
    }
    switch (iType)
    {
    case 0:
        iProtocol = Protocol_1_();
        break;
    case 1:
        iProtocol = Protocol_2_();
        break;
    case 2:
        iProtocol = Protocol_3_();
        break;
    case 3:
    default:
        iProtocol = Protocol_4_(aPath);

    }
#if defined (Debug)//Данный режим для отладки не  имеет положительного завершения приложения
    if(iProtocol)
    {
        Tps_Device_Protocol rfifo;
        Tps_Device_Protocol wfifo;
        rfifo.StartReadLoop();
        wfifo.OpenWriteMeasure();


        if(wfifo.Write(iProtocol))
        {
            iProtocol->DebugDesplayKadr();
             while(true);
        }
        else
            cout<<"Ошибка записи протокола";
        return -1;
    }

#else
    if(iProtocol)
    {
        Tps_Device_Protocol wfifo;
        wfifo.OpenWriteMeasure();
        if(wfifo.Write(iProtocol))
            iProtocol->DebugDesplayKadr();
        else
        {
            cout<<"Ошибка записи протокола";
            return -1;
        }
        return 0;
    }

#endif
    return -1;

}



