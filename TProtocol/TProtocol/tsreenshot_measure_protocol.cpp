#include "tsreenshot_measure_protocol.h"
using namespace pspProtocol;
//using namespace pspProtocol::ScreenshotTypeBuffer;
#include <string>

TScreenshot_Measure_Value::TScreenshot_Measure_Value(ScreenshotTypeBuffer *aBuffer):FBuffer(aBuffer)
{
    if(0<aBuffer->value().size())
    {
        int iWidth = aBuffer->mutable_size()->width();
        int iHeight = aBuffer->mutable_size()->height();
        FImage = QImage(iWidth,iHeight,QImage::Format_RGB32);
        for( int line = 0; line < FImage.height(); ++line )
            memcpy( FImage.scanLine(line), aBuffer->value().data()+line*FImage.bytesPerLine(), FImage.bytesPerLine() );
    }

}

const QImage& TScreenshot_Measure_Value::Screenshot()const
{
    return FImage;
}
bool TScreenshot_Measure_Value::Screenshot(QString aFileName)
{
    QImage lImage;
    if(lImage.load(aFileName))
    {
        if((0<lImage.width())&&(0<lImage.height()))
        {
            if(lImage.format()!=QImage::Format_RGB32)
                lImage = lImage.convertToFormat(QImage::Format_RGB32);
            FImage = lImage;
            pspProtocol::ScreenshotTypeBuffer_Size *iSize = FBuffer->mutable_size();
            iSize->set_width(FImage.width());
            iSize->set_height(FImage.height());
            std::string Bits;
            Bits.resize(FImage.bytesPerLine()*FImage.height());
            for(int line = 0; line < FImage.height(); ++line)
                memcpy(Bits.data()+line*FImage.bytesPerLine(),  FImage.scanLine(line), FImage.bytesPerLine() );
            FBuffer->set_value(Bits);
            return true;
        }

    }
    return false;
}
bool TScreenshot_Measure_Value::Screenshot(const QImage& aValue)
{
    if((0<aValue.width())&&(0<aValue.height()))
    {
        if(aValue.format()!=QImage::Format_RGB32)
            FImage = aValue.convertToFormat(QImage::Format_RGB32);
        else
            FImage = aValue;
        pspProtocol::ScreenshotTypeBuffer_Size *iSize = FBuffer->mutable_size();
        iSize->set_width(FImage.width());
        iSize->set_height(FImage.height());
        std::string Bits;
        Bits.resize(FImage.bytesPerLine()*FImage.height());
        for(int line = 0; line < FImage.height(); ++line)
            memcpy(Bits.data()+line*FImage.bytesPerLine(),  FImage.scanLine(line), FImage.bytesPerLine() );
        FBuffer->set_value(Bits);
        return true;
    }
    return false;
}

TScreenshot_Measure_Protocol::TScreenshot_Measure_Protocol(const QString &aDevece, const QString &aVersion):Tps_Header_Protocol(aDevece,aVersion)
{
    FKadr.set_type(pspProtocol::Kadr::screenshot);
    pspProtocol::ScreenshotTypeBuffer * newScreenshot = FKadr.mutable_screenshotmesure();
    pspProtocol::ScreenshotTypeBuffer_Size *iSize = newScreenshot->mutable_size();
    iSize->set_width(0);
    iSize->set_height(0);
    FScreenshot = new TScreenshot_Measure_Value(newScreenshot);
}

TScreenshot_Measure_Protocol::TScreenshot_Measure_Protocol(Kadr &aKadr)
{   
    FKadr = aKadr;    
    pspProtocol::ScreenshotTypeBuffer *cc = aKadr.mutable_screenshotmesure();
    FScreenshot = new TScreenshot_Measure_Value(FKadr.mutable_screenshotmesure());
}

TScreenshot_Measure_Protocol::~TScreenshot_Measure_Protocol()
{
    delete FScreenshot;
}

Tps_Header_Protocol<TScreenshot_Measure_Value>::TType TScreenshot_Measure_Protocol::Type() const
{
    return ptScreenshot;
}

TScreenshot_Measure_Value* TScreenshot_Measure_Protocol::Content()
{
    return FScreenshot;
}
