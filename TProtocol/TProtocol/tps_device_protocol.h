#ifndef TPS_DEVICE_PROTOCOL_H
#define TPS_DEVICE_PROTOCOL_H

#include <QThread>
#include <fcntl.h>
#include <atomic>
#include "tsingle_measure_protocol.h"
#include "trippler_mesure_protocol.h"
#include "tmeasure_drm_protocol.h"
#include "tsreenshot_measure_protocol.h"

class Tps_Device_Protocol : public QObject
{
    Q_OBJECT
public:
    explicit Tps_Device_Protocol(QObject* aOwner=nullptr);
    virtual ~Tps_Device_Protocol();
    //bool OpenReadMeasure(QString aFileName=QString());
    bool OpenWriteMeasure(QString aFileName=QString());
    bool Write(TObject_Measure_Protocol *aValue);
    bool StartReadLoop(QString aFileName=QString());
signals:
    void ReadSingleMeasure(TSingleMeasureProtocol *aValue);
    void ReadTripplerMeasure(Trippler_Mesure_Protocol *aValue);
    void ReadDRMMeasure(TMeasure_DRM_Protocol *aValue);
    void ReadScreenshotMeasure(TScreenshot_Measure_Protocol *aValue);
private slots:
    void Read();
private:
    QThread FThread;
    std::atomic<int>F_fd_FIFO;
    QString FFileFIFOPath;
    int FPipe_Size_Buffer;
    std::atomic<bool>FTerminate;
};

#endif // TPS_DEVICE_PROTOCOL_H
