#ifndef TRIPPLER_MESURE_PROTOCOL_H
#define TRIPPLER_MESURE_PROTOCOL_H

#include "tps_header_protocol.h"
#include "tmeasure.h"
class Trippler_Items_Mesure
{
public:
    Trippler_Items_Mesure(pspProtocol::TrippleTypeBuffer* aBuffer);
    virtual ~Trippler_Items_Mesure();
    TMeasure* Frist_Measure_Value();
    TMeasure* Second_Measure_Value();
    TMeasure* Third_Measure_Value();
private:
    TMeasure* FFrist_Measure_Value;
    TMeasure* FSecond_Measure_Value;
    TMeasure* FThird_Measure_Value;
};

class Trippler_Mesure_Protocol : public Tps_Header_Protocol<Trippler_Items_Mesure>
{
public:
    Trippler_Mesure_Protocol(const QString aDevece, const QString aVersion);
    Trippler_Mesure_Protocol(const pspProtocol::Kadr &aKadr);
    virtual ~Trippler_Mesure_Protocol();
    virtual Trippler_Items_Mesure* Content()override;
    TType Type()const override final;


private:
   Trippler_Items_Mesure *FItems;
};



#endif // TRIPPLER_MESURE_PROTOCOL_H
