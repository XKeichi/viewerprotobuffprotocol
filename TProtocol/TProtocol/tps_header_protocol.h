#ifndef TPS_HEADER_PROTOCOL_H
#define TPS_HEADER_PROTOCOL_H

#include <qtextcodec.h>
#include "Protocol.pb.h"
#include <string>

class TObject_Measure_Protocol
{
public:
    void Serialize(std::string &rBuffer);
    void DebugDesplayKadr();
protected:
    pspProtocol::Kadr FKadr;
};


template<typename TContent>class Tps_Header_Protocol : public TObject_Measure_Protocol
{
public:
    enum TType { ptUndefine=-1, ptSingleR  = 0, ptTrippleR = 1, ptDRM = 2, ptScreenshot = 3};

public:
    explicit Tps_Header_Protocol(const QString &aDevece, const QString &aVersion);
    explicit Tps_Header_Protocol(){}
    virtual ~Tps_Header_Protocol();
    const QString Device()const;
    const QString Version()const;
    virtual TType Type()const=0;
    virtual TContent* Content()=0;   
protected:

};

template<typename TContent>Tps_Header_Protocol<TContent>::Tps_Header_Protocol(const QString &aDevece, const QString &aVersion)
{
     GOOGLE_PROTOBUF_VERIFY_VERSION;
     QTextCodec *iCodec = QTextCodec::codecForName("UTF-8");
     QTextCodec::setCodecForLocale(iCodec);
     FKadr.set_device(aDevece.toLocal8Bit().data());
     FKadr.set_version(aVersion.toLocal8Bit().data());
     //FType = ptUndefine;
}

template<typename TContent>Tps_Header_Protocol<TContent>::~Tps_Header_Protocol()
{
    google::protobuf::ShutdownProtobufLibrary();
}

template<typename TContent>const QString Tps_Header_Protocol<TContent>::Device()const
{
    return QString::fromLocal8Bit(FKadr.device().data());
}

template<typename TContent>const QString Tps_Header_Protocol<TContent>::Version()const
{
    return QString::fromLocal8Bit(FKadr.version().data());
}

#endif // TPS_HEADER_PROTOCOL_H
