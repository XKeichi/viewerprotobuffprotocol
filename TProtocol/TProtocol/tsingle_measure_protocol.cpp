#include "tsingle_measure_protocol.h"
using namespace pspProtocol;
TSingleMeasureProtocol::TSingleMeasureProtocol(const QString &aDevece, const QString &aVersion):Tps_Header_Protocol(aDevece,aVersion)
{
    FKadr.set_type(pspProtocol::Kadr::singleR);
    FMeasure = new TMeasure(FKadr.mutable_singlemesure()->mutable_value());
}

TSingleMeasureProtocol::TSingleMeasureProtocol(const pspProtocol::Kadr &aKadr)
{
    FKadr = aKadr;
    FMeasure = new TMeasure(FKadr.mutable_singlemesure()->mutable_value());
}

TSingleMeasureProtocol::~TSingleMeasureProtocol()
{
    delete FMeasure;
}

Tps_Header_Protocol<TMeasure>::TType TSingleMeasureProtocol::Type() const
{
    return ptSingleR;
}

TMeasure *TSingleMeasureProtocol::Content()
{
    return FMeasure;
}
