#include "trippler_mesure_protocol.h"
using namespace pspProtocol;
Trippler_Items_Mesure::Trippler_Items_Mesure(pspProtocol::TrippleTypeBuffer* aBuffer)
{
    FFrist_Measure_Value = new TMeasure(aBuffer->mutable_value1());
    FSecond_Measure_Value = new TMeasure(aBuffer->mutable_value2());
    FThird_Measure_Value = new TMeasure(aBuffer->mutable_value3());
}
Trippler_Items_Mesure::~Trippler_Items_Mesure()
{
    delete FFrist_Measure_Value;
    delete FSecond_Measure_Value;
    delete FThird_Measure_Value;
}
TMeasure* Trippler_Items_Mesure::Frist_Measure_Value()
{
    return FFrist_Measure_Value;
}
TMeasure* Trippler_Items_Mesure::Second_Measure_Value()
{
    return FSecond_Measure_Value;
}
TMeasure* Trippler_Items_Mesure::Third_Measure_Value()
{
    return FThird_Measure_Value;
}

Trippler_Mesure_Protocol::Trippler_Mesure_Protocol(const QString aDevece, const QString aVersion):Tps_Header_Protocol(aDevece,aVersion)
{
    FKadr.set_type(pspProtocol::Kadr::trippleR);
    pspProtocol::TrippleTypeBuffer* iBuffer = FKadr.mutable_triplemesures();
    FItems = new Trippler_Items_Mesure(iBuffer);
}

Trippler_Mesure_Protocol::Trippler_Mesure_Protocol(const pspProtocol::Kadr &aKadr)
{
    FKadr = aKadr;
    pspProtocol::TrippleTypeBuffer* iBuffer = FKadr.mutable_triplemesures();
    FItems = new Trippler_Items_Mesure(iBuffer);
}

Trippler_Mesure_Protocol::~Trippler_Mesure_Protocol()
{
    delete FItems;
}

Trippler_Items_Mesure* Trippler_Mesure_Protocol::Content()
{
    return FItems;
}

Tps_Header_Protocol<Trippler_Items_Mesure>::TType Trippler_Mesure_Protocol::Type() const
{
    return ptTrippleR;
}
