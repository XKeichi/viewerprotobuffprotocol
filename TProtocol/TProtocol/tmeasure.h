#ifndef TMEASURE_H
#define TMEASURE_H

#include<QString>

#include "Protocol.pb.h"

class TMeasure
{
public:    
    TMeasure(pspProtocol::Mesure *aMesure);
    TMeasure(const TMeasure &aValue);
    float Amperage()const;
    float Voltage() const;
    QString AmperageDisplayValue(unsigned int aPrecision=3) const;
    QString VoltageDisplayValue(unsigned int aPrecision=3)  const;
    void Amperage(float aValue);
    void Voltage(float aValue);
    bool operator == (const TMeasure& aValue)const;
    bool operator != (const TMeasure& aValue)const;
    TMeasure& operator = (const TMeasure &aValue);
private:
    pspProtocol::Mesure *FMesure;
private:
    TMeasure(const TMeasure&&)=delete;


};

#endif // TMEASURE_H
