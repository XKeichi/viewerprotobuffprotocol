#ifndef TMEASURE_DRM_PROTOCOL_H
#define TMEASURE_DRM_PROTOCOL_H

#include "tmeasure.h"
#include "tps_header_protocol.h"
#include <QVector>
class TList_Measure_Value
{
public:
    TList_Measure_Value(pspProtocol::DRMTypeBuffer *aBuffer);
    ~TList_Measure_Value();
    const QVector<TMeasure*>& Items()const;
    TMeasure* CreateNewMeasure();
private:
     pspProtocol::DRMTypeBuffer *FBuffer;
     QVector<TMeasure*>FItems;

};

class TMeasure_DRM_Protocol : public Tps_Header_Protocol<TList_Measure_Value>
{
public:
    TMeasure_DRM_Protocol(const QString &aDevece, const QString &aVersion);
    TMeasure_DRM_Protocol(const pspProtocol::Kadr &aKadr);
    virtual ~TMeasure_DRM_Protocol();
    virtual  TList_Measure_Value* Content()override;
    TType Type()const override final;
private:
    TList_Measure_Value *FItems;
};

#endif // TMEASURE_DRM_PROTOCOL_H
