#include "tps_device_protocol.h"
#include <qdir.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>

#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

Tps_Device_Protocol::Tps_Device_Protocol(QObject *aOwner):QObject(aOwner)
{
    F_fd_FIFO.store(-1);
    FPipe_Size_Buffer = 0;
    FTerminate.store(false);
}
Tps_Device_Protocol::~Tps_Device_Protocol()
{
    if(FThread.isRunning())
    {
        FTerminate.store(true);
        while(FThread.isRunning())
        {
            FThread.wait(100);
        }




    }
    if(F_fd_FIFO.load()!=-1)
        close(F_fd_FIFO.load());
    if(QFile::exists(FFileFIFOPath))
        unlink(FFileFIFOPath.toLocal8Bit().data());
}

inline bool CreateFIFOFile(QString &aFileName)
{
    if(aFileName.isEmpty())
    {
        aFileName = QDir::tempPath();
        if(QDir(aFileName+"/Personal Studio").exists()==false)
        {
            if(QDir::temp().mkdir("Personal Studio")==false)
                return false;
        }
        aFileName+="/Personal Studio/pb_Multimeter_Protocol.fifo";
    }
    if(QFile::exists(aFileName)==false)
    {
        unsigned int FIFO_MODE =S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP /*| S_IROTH | S_IWOTH*/;

        if(mkfifo(aFileName.toLocal8Bit().data(), O_RDWR|FIFO_MODE)==-1)
            return false;
    }
    return true;
}


bool Tps_Device_Protocol::OpenWriteMeasure(QString aFileName)
{

    if(F_fd_FIFO.load()!=-1)
    {
        close(F_fd_FIFO.load());
        F_fd_FIFO = -1;
    }
    if(CreateFIFOFile(aFileName))
    {
        F_fd_FIFO.store(open(aFileName.toLocal8Bit().data(),O_RDWR));
        if(F_fd_FIFO.load()==-1)
            return false;
        FFileFIFOPath = aFileName;
        FPipe_Size_Buffer = pathconf(aFileName.toLocal8Bit().data(), _PC_PIPE_BUF);
        return true;
    }
    return false;
}

bool Tps_Device_Protocol::Write(TObject_Measure_Protocol *aValue)
{
    if(F_fd_FIFO.load()!=-1)
    {        
        std::string Buffer;
        aValue->Serialize(Buffer);
        char Marker[8] = {(char)0xDE,(char)0xAD,(char)0xBE,(char)0xEF};
        int Size = Buffer.size();
        memccpy(Marker+4,&Size,1,sizeof(int));
        Buffer = std::string(Marker,8)+Buffer;
        int Positiom;
        for(uint64_t iSTep=0;iSTep<Buffer.size()/FPipe_Size_Buffer;++iSTep)
        {
            Positiom = iSTep*FPipe_Size_Buffer;
            if(write(F_fd_FIFO.load(),Buffer.data()+Positiom,FPipe_Size_Buffer)<FPipe_Size_Buffer)
                return false;
        }
        int rem = Buffer.size()%FPipe_Size_Buffer;
        if(0<rem)
        {
            Positiom = Buffer.size()/FPipe_Size_Buffer*FPipe_Size_Buffer;
            if(write(F_fd_FIFO.load(),Buffer.data()+Positiom,rem)<rem)
                return false;
        }
        return true;


    }
    return false;

}

bool Tps_Device_Protocol::StartReadLoop(QString aFileName)
{

    if(F_fd_FIFO.load()!=-1)
    {
        close(F_fd_FIFO.load());
        F_fd_FIFO.store(-1);
    }
    if(FThread.isRunning())
    {
        FTerminate.store(true);
        unsigned int Counter=0;
        while(FThread.isRunning())
        {
            FThread.wait(100);
            Counter++;
            if(Counter==100)
                return false;

        }
    }
    if(CreateFIFOFile(aFileName))
    {
        FFileFIFOPath = aFileName;
        FTerminate.store(false);
        moveToThread(&FThread);
        connect(&FThread,SIGNAL(started()),SLOT(Read()));
        FThread.start();
        return true;
    }
    return false;

}

void Tps_Device_Protocol::Read()
{
    const unsigned int bSize = 3*1024;
    const char Marker[] = {(char)0xDE,(char)0xAD,(char)0xBE,(char)0xEF};
    char Buffer[bSize];
    unsigned int rSize=0;
    unsigned int Current_Size_Kadr=0;
    unsigned int CountCopyBytes;
    pspProtocol::Kadr iValue;
    std::string iMessure;
    F_fd_FIFO.store(open(FFileFIFOPath.toLocal8Bit().data(),O_RDONLY));
    if(F_fd_FIFO.load()==-1)
        return;
    while(FTerminate.load()==false)
    {
        CountCopyBytes=0;
        if(0<(rSize=read(F_fd_FIFO.load(),Buffer,bSize)))
        {
            if((Current_Size_Kadr==0)&&(memcmp(Buffer,Marker,4)==0))
            {
                memccpy(&Current_Size_Kadr,Buffer+4,1,sizeof(int));
                CountCopyBytes = (Current_Size_Kadr<=(rSize-8))?Current_Size_Kadr:(rSize-8);
                iMessure += std::string(Buffer+8,CountCopyBytes);
            }
            else
            {
                if(0<Current_Size_Kadr)
                {
                    CountCopyBytes = (Current_Size_Kadr<=rSize)?Current_Size_Kadr:rSize;
                    iMessure += std::string(Buffer,CountCopyBytes);
                }
            }
            if((0<Current_Size_Kadr)&&(0<CountCopyBytes))
            {
                Current_Size_Kadr-=CountCopyBytes;
                if(Current_Size_Kadr==0)
                {

                    iValue.ParseFromString(iMessure);
                    FThread.exit(0);
                    close(F_fd_FIFO.load());
                    F_fd_FIFO.store(-1);
                    switch(iValue.type())
                    {
                    case pspProtocol::Kadr::singleR:
                        emit ReadSingleMeasure(new TSingleMeasureProtocol(iValue));
                        break;
                    case pspProtocol::Kadr::trippleR:
                        emit ReadTripplerMeasure(new Trippler_Mesure_Protocol(iValue));
                        break;
                    case pspProtocol::Kadr::DRM:
                        emit ReadDRMMeasure(new TMeasure_DRM_Protocol(iValue));
                        break;
                    case pspProtocol::Kadr::screenshot:
                        emit ReadScreenshotMeasure(new TScreenshot_Measure_Protocol(iValue));
                        break;                       
                    }


                    return;
                }
            }


        }
    }

}
