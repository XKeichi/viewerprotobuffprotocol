INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD
LIBS +=-lprotobuf
SOURCES += \
        $$PWD/Protocol.pb.cc \
        $$PWD/tmeasure.cpp \
        $$PWD/tmeasure_drm_protocol.cpp \
        $$PWD/tps_device_protocol.cpp \
        $$PWD/tps_header_protocol.cpp \
        $$PWD/trippler_mesure_protocol.cpp \
        $$PWD/tsingle_measure_protocol.cpp \
        $$PWD/tsreenshot_measure_protocol.cpp
        
HEADERS += \
    $$PWD/Protocol.pb.h \
    $$PWD/tmeasure.h \
    $$PWD/tmeasure_drm_protocol.h \
    $$PWD/tps_device_protocol.h \
    $$PWD/tps_header_protocol.h \
    $$PWD/trippler_mesure_protocol.h \
    $$PWD/tsingle_measure_protocol.h \
    $$PWD/tsreenshot_measure_protocol.h
        
