#ifndef TMAIN_WINDOW_APPLICATION_H
#define TMAIN_WINDOW_APPLICATION_H

#include <QMainWindow>
#include <tps_device_protocol.h>
QT_BEGIN_NAMESPACE
namespace Ui { class TMain_Window_Application; }
QT_END_NAMESPACE

class TMain_Window_Application : public QMainWindow
{
    Q_OBJECT

public:
    TMain_Window_Application(QWidget *parent = nullptr);
    ~TMain_Window_Application();
    Tps_Device_Protocol FIFO;
private slots:
    void on_actionExitApplication_triggered();
    void ReadSingleMeasure(TSingleMeasureProtocol *aValue);
    void ReadTripplerMeasure(Trippler_Mesure_Protocol *aValue);
    void ReadDRMMeasure(TMeasure_DRM_Protocol *aValue);
    void ReadScreenshotMeasure(TScreenshot_Measure_Protocol *aValue);
    void on_actionFIFOOPen_triggered();

private:
    Ui::TMain_Window_Application *ui;    
};
#endif // TMAIN_WINDOW_APPLICATION_H
