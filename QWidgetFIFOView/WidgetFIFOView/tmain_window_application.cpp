#include "tmain_window_application.h"
#include "ui_tmain_window_application.h"
#include <QLabel>
#include <QTableWidget>
#include <QTreeWidget>
#include <QHeaderView>
#include <QMessageBox>
inline void ClearUIValue(QLayout *aOwner)
{
    while(0<aOwner->count())
    {
        QWidget *UIWidget = aOwner->itemAt(0)->widget();
        if(UIWidget)
        {
            aOwner->removeWidget(UIWidget);
            delete UIWidget;
            UIWidget = NULL;
        }
        else
        {
            aOwner->removeItem(aOwner->itemAt(0));
        }

    }
}

TMain_Window_Application::TMain_Window_Application(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::TMain_Window_Application)
{
    ui->setupUi(this);
    connect(&FIFO,SIGNAL(ReadDRMMeasure(TMeasure_DRM_Protocol*)),SLOT(ReadDRMMeasure(TMeasure_DRM_Protocol*)));
    connect(&FIFO,SIGNAL(ReadScreenshotMeasure(TScreenshot_Measure_Protocol*)),SLOT(ReadScreenshotMeasure(TScreenshot_Measure_Protocol*)));
    connect(&FIFO,SIGNAL(ReadSingleMeasure(TSingleMeasureProtocol*)),SLOT(ReadSingleMeasure(TSingleMeasureProtocol*)));
    connect(&FIFO,SIGNAL(ReadTripplerMeasure(Trippler_Mesure_Protocol*)),SLOT(ReadTripplerMeasure(Trippler_Mesure_Protocol*)));

}


TMain_Window_Application::~TMain_Window_Application()
{
    delete ui;
}

void TMain_Window_Application::on_actionExitApplication_triggered()
{
    close();
}

void TMain_Window_Application::ReadSingleMeasure(TSingleMeasureProtocol *aValue)
{
    resize(800,600);
    setWindowTitle("Учстройство="+aValue->Device()+", версия="+aValue->Version());
    ClearUIValue(ui->Align);
    QLabel *iLabel;

    iLabel = new QLabel("Сопротивление = "+aValue->Content()->VoltageDisplayValue());
    iLabel->setAlignment(Qt::AlignCenter);
    ui->Align->addWidget(iLabel);
    iLabel = new QLabel("Сила тока = "+aValue->Content()->AmperageDisplayValue());
    iLabel->setAlignment(Qt::AlignCenter);
    ui->Align->addWidget(iLabel);
    qApp->processEvents();

}
void TMain_Window_Application::ReadTripplerMeasure(Trippler_Mesure_Protocol *aValue)
{
    resize(800,600);
    setWindowTitle("Учстройство="+aValue->Device()+", версия="+aValue->Version());
    ClearUIValue(ui->Align);
    QTreeWidget *tw = new QTreeWidget;
    tw->setColumnCount(2);
    tw->headerItem()->setText(0,"Параметр");
    tw->headerItem()->setText(1,"Значение");
    QTreeWidgetItem *nRoot = new QTreeWidgetItem(tw);
    nRoot->setText(0,"Измерения");
    //chanel 1
    QTreeWidgetItem *nChanel = new QTreeWidgetItem(nRoot);
    nChanel->setText(0,"Канал -1");
    QTreeWidgetItem *nParam = new QTreeWidgetItem(nChanel);
    nParam->setText(0,"Сопротивление");
    nParam->setText(1,aValue->Content()->Frist_Measure_Value()->VoltageDisplayValue());
    nParam = new QTreeWidgetItem(nChanel);
    nParam->setText(0,"Сила тока");
    nParam->setText(1,aValue->Content()->Frist_Measure_Value()->AmperageDisplayValue());
    //chanel 2
    nChanel = new QTreeWidgetItem(nRoot);
    nChanel->setText(0,"Канал -2");
    nParam = new QTreeWidgetItem(nChanel);
    nParam->setText(0,"Сопротивление");
    nParam->setText(1,aValue->Content()->Second_Measure_Value()->VoltageDisplayValue());
    nParam = new QTreeWidgetItem(nChanel);
    nParam->setText(0,"Сила тока");
    nParam->setText(1,aValue->Content()->Second_Measure_Value()->AmperageDisplayValue());
    //chanel 3
    nChanel = new QTreeWidgetItem(nRoot);
    nChanel->setText(0,"Канал -3");
    nParam = new QTreeWidgetItem(nChanel);
    nParam->setText(0,"Сопротивление");
    nParam->setText(1,aValue->Content()->Third_Measure_Value()->VoltageDisplayValue());
    nParam = new QTreeWidgetItem(nChanel);
    nParam->setText(0,"Сила тока");
    nParam->setText(1,aValue->Content()->Third_Measure_Value()->AmperageDisplayValue());    
    ui->Align->addWidget(tw);
    tw->expandAll();
    qApp->processEvents();

}
void TMain_Window_Application::ReadDRMMeasure(TMeasure_DRM_Protocol *aValue)
{
    resize(800,600);
    setWindowTitle("Учстройство="+aValue->Device()+", версия="+aValue->Version());
    ClearUIValue(ui->Align);
    QTableWidget *tw = new QTableWidget;
    tw->setColumnCount(2);
    tw->setRowCount(aValue->Content()->Items().size());
    QTableWidgetItem *iCell = nullptr;
    iCell = new QTableWidgetItem("Сопротивление");
    tw->setHorizontalHeaderItem(0,iCell);
    iCell = new QTableWidgetItem("Сила тока");
    tw->setHorizontalHeaderItem(1,iCell);
    tw->horizontalHeader()->setDefaultSectionSize(width()/2-30);
    tw->horizontalHeader()->setStretchLastSection(true);
    auto pMeasure=aValue->Content()->Items().begin();
    for(int iRow=0;iRow<tw->rowCount();++iRow,++pMeasure)
    {
        iCell = new QTableWidgetItem((*pMeasure)->VoltageDisplayValue());
        tw->setItem(iRow,0,iCell);
        iCell = new QTableWidgetItem((*pMeasure)->AmperageDisplayValue());
        tw->setItem(iRow,1,iCell);
    }
    tw->verticalHeader()->setVisible(false);
    ui->Align->addWidget(tw);
    qApp->processEvents();

}
void TMain_Window_Application::ReadScreenshotMeasure(TScreenshot_Measure_Protocol *aValue)
{

    setWindowTitle("Учстройство="+aValue->Device()+", версия="+aValue->Version());
    ClearUIValue(ui->Align);
    QLabel *Image = new QLabel;
    QImage iScreenshot = aValue->Content()->Screenshot().scaled(width()-18,height()-18);
    QSize cc[]={iScreenshot.size(),aValue->Content()->Screenshot().size()};
    Image->setPixmap(QPixmap::fromImage(iScreenshot));
    ui->Align->addWidget(Image);
    qApp->processEvents();
}

void TMain_Window_Application::on_actionFIFOOPen_triggered()
{

    if(FIFO.StartReadLoop()==false)
    {
        QMessageBox uiError;
        uiError.setText("Ошибка открытие именного канала FIFO");
        uiError.exec();
        exit(-1);
    }
    else
    {
        ClearUIValue(ui->Align);
        resize(800,600);
        QLabel *iLablel = new QLabel("Слушаю FIFO канал");
        ui->Align->addWidget(iLablel);
        iLablel->setAlignment(Qt::AlignCenter);
    }
}
