#include "tmain_window_application.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TMain_Window_Application w;
    w.show();
    return a.exec();
}
