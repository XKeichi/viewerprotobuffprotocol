#ifndef TDISPLAYFIFOMEASURE_H
#define TDISPLAYFIFOMEASURE_H

#include <QObject>
#include <tps_device_protocol.h>
#include <tlist_measure_value.h>
class TDisplayFIFOMeasure : public QObject
{
    Q_OBJECT    
    Q_PROPERTY(TModel_Measure_Display* measures READ getMeasureModelValue NOTIFY changeModelMeasure)
public:
    explicit TDisplayFIFOMeasure(QObject *parent = nullptr);
    Q_INVOKABLE bool openFIFOMeasure();
    Q_INVOKABLE QString device();
    Q_INVOKABLE QString version();
signals:
    void readSingleMeasure();
    void readTripplerMeasure();
    void readDRMMeasure();
    void readScreenshotMeasure();
    void changeModelMeasure();
private:
    Tps_Device_Protocol FFIFO;
    QString FDevice;
    QString FVersion;
    TModel_Measure_Display FMeaseres;
private:
    TModel_Measure_Display* getMeasureModelValue();
private slots:
    void ReadSingleMeasure(TSingleMeasureProtocol *aValue);
    void ReadTripplerMeasure(Trippler_Mesure_Protocol *aValue);
    void ReadDRMMeasure(TMeasure_DRM_Protocol *aValue);
    void ReadScreenshotMeasure(TScreenshot_Measure_Protocol *aValue);

};

#endif // TDISPLAYFIFOMEASURE_H
