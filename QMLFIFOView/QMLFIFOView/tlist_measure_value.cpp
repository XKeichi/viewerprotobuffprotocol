#include "tlist_measure_value.h"

TModel_Measure_Display::TModel_Measure_Display(QObject *parent) : QAbstractListModel(parent)
{
    FRoleNames[drAmperage] = "amper";
    FRoleNames[drVoltage] = "om";
}

void TModel_Measure_Display::Clear()
{
    std::for_each(FValues.begin(), FValues.end(),[](TItem *&iValue){delete iValue;iValue=nullptr;});
    FValues.clear();
}

TModel_Measure_Display::~TModel_Measure_Display()
{
    Clear();
}

QString TModel_Measure_Display::TItem::Amperage()
{
    return FAmperage;
}
QString TModel_Measure_Display::TItem::Voltage()
{
    return FVoltage;
}

int TModel_Measure_Display::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return FValues.size();
}
QVariant TModel_Measure_Display::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if(row < 0 || row >= FValues.size())
    {
        return QVariant();
    }
    switch (role) {
    case drAmperage:
       return FValues[row]->Amperage();
        break;
    case drVoltage:
       return FValues[row]->Voltage();
        break;
    default:
        break;
    }
    return QVariant();

}

void TModel_Measure_Display::Set(TSingleMeasureProtocol *aValue)
{
    Clear();
    FValues.push_back(new TItem(aValue->Content()->AmperageDisplayValue(),aValue->Content()->VoltageDisplayValue()));
}
void TModel_Measure_Display::Set(Trippler_Mesure_Protocol *aValue)
{
    Clear();
    FValues.push_back(new TItem(aValue->Content()->Frist_Measure_Value()->AmperageDisplayValue(),
                                aValue->Content()->Frist_Measure_Value()->VoltageDisplayValue()));
    FValues.push_back(new TItem(aValue->Content()->Second_Measure_Value()->AmperageDisplayValue(),
                                aValue->Content()->Second_Measure_Value()->VoltageDisplayValue()));
    FValues.push_back(new TItem(aValue->Content()->Third_Measure_Value()->AmperageDisplayValue(),
                                aValue->Content()->Third_Measure_Value()->VoltageDisplayValue()));
}
void TModel_Measure_Display::Set(TMeasure_DRM_Protocol *aValue)
{
    Clear();
    FValues.resize(aValue->Content()->Items().size());
    auto iValue = FValues.begin();
    for(auto iMeasure : aValue->Content()->Items())
    {
        *iValue = new TItem(iMeasure->AmperageDisplayValue(),iMeasure->VoltageDisplayValue());
        iValue++;
    }
}

QHash<int,QByteArray>TModel_Measure_Display::roleNames()const
{
    return FRoleNames;
}
