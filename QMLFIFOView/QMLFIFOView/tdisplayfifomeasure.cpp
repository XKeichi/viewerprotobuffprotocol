#include "tdisplayfifomeasure.h"
#include "tscreenshot_image_provider.h"
#include <qdebug.h>
TDisplayFIFOMeasure::TDisplayFIFOMeasure(QObject *parent) : QObject(parent)
{
    connect(&FFIFO,SIGNAL(ReadDRMMeasure(TMeasure_DRM_Protocol*)),SLOT(ReadDRMMeasure(TMeasure_DRM_Protocol*)));
    connect(&FFIFO,SIGNAL(ReadScreenshotMeasure(TScreenshot_Measure_Protocol*)),SLOT(ReadScreenshotMeasure(TScreenshot_Measure_Protocol*)));
    connect(&FFIFO,SIGNAL(ReadSingleMeasure(TSingleMeasureProtocol*)),SLOT(ReadSingleMeasure(TSingleMeasureProtocol*)));
    connect(&FFIFO,SIGNAL(ReadTripplerMeasure(Trippler_Mesure_Protocol*)),SLOT(ReadTripplerMeasure(Trippler_Mesure_Protocol*)));
}


void TDisplayFIFOMeasure::ReadSingleMeasure(TSingleMeasureProtocol *aValue)
{
    FVersion = aValue->Version();
    FDevice  = aValue->Device();
    FMeaseres.Set(aValue);
    emit readSingleMeasure();
    qDebug()<<"emit cpp";


}
void TDisplayFIFOMeasure::ReadTripplerMeasure(Trippler_Mesure_Protocol *aValue)
{
    FVersion = aValue->Version();
    FDevice  = aValue->Device();
    FMeaseres.Set(aValue);
    emit readTripplerMeasure();


}
void TDisplayFIFOMeasure::ReadDRMMeasure(TMeasure_DRM_Protocol *aValue)
{
    FVersion = aValue->Version();
    FDevice  = aValue->Device();
    FMeaseres.Set(aValue);
    emit readDRMMeasure();


}
void TDisplayFIFOMeasure::ReadScreenshotMeasure(TScreenshot_Measure_Protocol *aValue)
{
    FVersion = aValue->Version();
    FDevice  = aValue->Device();
    TScreenshot_Image_Provider::Hinstance()->Screenshot(aValue);
    emit readScreenshotMeasure();
}

bool TDisplayFIFOMeasure::openFIFOMeasure()
{
    return FFIFO.StartReadLoop();
}

TModel_Measure_Display* TDisplayFIFOMeasure::getMeasureModelValue()
{
    return &FMeaseres;
}

QString TDisplayFIFOMeasure::device()
{
    return FDevice;
}
QString TDisplayFIFOMeasure::version()
{
    return FVersion;
}
