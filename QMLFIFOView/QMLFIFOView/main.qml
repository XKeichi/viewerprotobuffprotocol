import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.3
import Personal.Studio 1.0

Window {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    PSProtocol
    {
        id: psProtocol
        onReadSingleMeasure:
        {
            window.title = "Прибор="+psProtocol.device()+", Версия="+psProtocol.version()
            uiDisplayPSProtocol.source = "ui_Single_Measure.qml"

        }
        onReadTripplerMeasure:
        {
            window.title = "Прибор="+psProtocol.device()+", Версия="+psProtocol.version()
            uiDisplayPSProtocol.source = "ui_Tripple_Measure.qml"
        }
        onReadDRMMeasure:
        {
            window.title = "Прибор="+psProtocol.device()+", Версия="+psProtocol.version()
            uiDisplayPSProtocol.source = "ui_DRM_Measure.qml"
        }
        onReadScreenshotMeasure:
        {
            window.title = "Прибор="+psProtocol.device()+", Версия="+psProtocol.version()
            uiDisplayPSProtocol.source = "ui_Screenshot_Measure.qml"
        }

    }


    ToolBar {
        id: toolBar
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0

        ToolButton {
            id: tbOpenFIFOChanel
            text: "Открыть канал  FIFO"
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            onClicked:
            {
                uiDisplayPSProtocol.sourceComponent = uiFIFOListen
                psProtocol.openFIFOMeasure();
            }
        }

        ToolButton {
            id: tbExit
            x: 517
            text: "Выход"
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            onClicked: Qt.quit();
        }

    }
    Loader
    {
        id:uiDisplayPSProtocol
        anchors.top: toolBar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        sourceComponent: uiPromt

    }
    Component
    {
        id: uiPromt
        Label
        {
            id:labelPromt;
            text: "Откройте канал FIFO"
            font.pixelSize: 22
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment  : Text.AlignVCenter
        }
    }
    Component
    {
        id: uiFIFOListen
        Label
        {
            id:labelPromt;
            text: "Готов принять данные из канала FIFO"
            font.pixelSize: 22
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment  : Text.AlignVCenter
        }
    }

}

