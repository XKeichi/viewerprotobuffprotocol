#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <memory>
#include "tdisplayfifomeasure.h"
#include "tscreenshot_image_provider.h"
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<TDisplayFIFOMeasure>("Personal.Studio",1,0,"PSProtocol");
    qmlRegisterType<TModel_Measure_Display>("Personal.Studio",1,0,"QML_Model_Measure");
    //std::weak_ptr ScreenshotPSProvider<TScreenshot_Image_Provider>;

    engine.addImageProvider("pspScreenshot",TScreenshot_Image_Provider::Hinstance());
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
