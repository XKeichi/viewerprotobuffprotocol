#include "tscreenshot_image_provider.h"
TScreenshot_Image_Provider* TScreenshot_Image_Provider::FHandle=nullptr;

TScreenshot_Image_Provider::~TScreenshot_Image_Provider()
{
    FHandle = NULL;
}

TScreenshot_Image_Provider* TScreenshot_Image_Provider::Hinstance()
{

    if(FHandle==nullptr)
    {
        FHandle = new TScreenshot_Image_Provider();
    }
    return FHandle;

}

QImage TScreenshot_Image_Provider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    Q_UNUSED(id);
    if(size)
    {
        *size = FScreenshot.size();
    }
    if(0<requestedSize.width()&&(0<requestedSize.height()))
    {
        return FScreenshot.scaled(requestedSize);
    }
    return FScreenshot;
}

void TScreenshot_Image_Provider::Screenshot(TScreenshot_Measure_Protocol *aScreenshotMeasureValue)
{
    FScreenshot=aScreenshotMeasureValue->Content()->Screenshot();
}

