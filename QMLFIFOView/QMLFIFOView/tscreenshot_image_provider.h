#ifndef TSCREENSHOT_IMAGE_PROVIDER_H
#define TSCREENSHOT_IMAGE_PROVIDER_H

#include <QQuickImageProvider>
#include <tsreenshot_measure_protocol.h>

class TScreenshot_Image_Provider : public QQuickImageProvider
{
public:
    static TScreenshot_Image_Provider* Hinstance();
    virtual~TScreenshot_Image_Provider();
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize)override;
    void Screenshot(TScreenshot_Measure_Protocol *aScreenshotMeasureValue);
private:
    TScreenshot_Image_Provider():QQuickImageProvider(QQuickImageProvider::Image){}
private:
    static TScreenshot_Image_Provider* FHandle;
    QImage FScreenshot;
};

#endif // TSCREENSHOT_IMAGE_PROVIDER_H
