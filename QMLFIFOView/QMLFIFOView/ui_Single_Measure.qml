import QtQuick 2.0
import Personal.Studio 1.0

ListView
{
    model: psProtocol.measures;
    id: lwSingleMeasure
    clip: true
    delegate: Column
    {
        width: parent.width;
        height: lwSingleMeasure.height
        Row
        {
            width: parent.width
            height: parent.height/2;
            Text
            {
                width: parent.width/2
                height: parent.height/2;
                id: titleAmperage
                text: qsTr("Сила тока:")
                verticalAlignment  : Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter

            }
            Text
            {
                width: parent.width/2
                height: parent.height/2;
                id: valueAmperage
                text: model.amper
                verticalAlignment  : Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

        }

        Row
        {
            width: parent.width
            height: parent.height/2;
            Text
            {
                width: parent.width/2
                height: parent.height/2;
                id: titleVoltage
                text: qsTr("Сопротивление:")
                verticalAlignment  : Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            Text
            {
                width: parent.width/2
                height: parent.height/2;
                id: valueVoltage
                text: model.om
                verticalAlignment  : Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }



    }
}
