import QtQuick.Controls 1.2
import Personal.Studio 1.0
TableView
{
    clip: true
    model: psProtocol.measures
    TableViewColumn
    {
        role: "om"
        title: "Сопротивление"
        width: parent.width/2
    }
    TableViewColumn
    {
        role: "amper"
        title: "Сила Тока"
        width: parent.width/2
    }


}
