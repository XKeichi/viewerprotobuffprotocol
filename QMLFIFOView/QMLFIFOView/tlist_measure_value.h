#ifndef TLIST_MEASURE_VALUE_H
#define TLIST_MEASURE_VALUE_H

#include <QAbstractListModel>
#include <QVector>
#include <tps_device_protocol.h>
class TModel_Measure_Display : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit TModel_Measure_Display(QObject *parent = nullptr);
    virtual ~TModel_Measure_Display();
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    void Set(TSingleMeasureProtocol *aValue);
    void Set(Trippler_Mesure_Protocol *aValue);
    void Set(TMeasure_DRM_Protocol *aValue);
protected:
    virtual QHash<int,QByteArray>roleNames()const override;
private:
    class TItem;
    enum TRoles { drAmperage = Qt::UserRole+1, drVoltage = Qt::UserRole+2 };
private:
    QVector<TItem*>FValues;
    QHash<int, QByteArray> FRoleNames;

private:
    void Clear();
signals:

};

class TModel_Measure_Display::TItem
{
public:
    TItem(QString Amperage, QString Voltage):FAmperage(Amperage),FVoltage(Voltage){}
    QString Amperage();
    QString Voltage();
private:
    QString FAmperage;
    QString FVoltage;
};

#endif // TLIST_MEASURE_VALUE_H
